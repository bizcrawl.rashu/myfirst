@extends('back.layout.master')
@section('content')

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    

           <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                <th>Id</th>
                <th>Users</th>
                <th>UserType</th>
                <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data  as $key=>$d)
  		        <tr>
                  <td scope="row">{{$data->firstItem()+$key}}</td>
                  <td>{{$d->User['name']}}</td>
                  <td>{{$d->Usertype['name']}}</td>
                
                  <td>
                  	<a href="{{route('role.edit',$d->id)}}" class="btn btn-secondary"><i class="fa fa-cogs"></i>Edit</a>
                     <a class="btn btn-secondary" style="border-color: none;">
                  	<form action="{{route('role.destroy',$d->id)}}" method="POST" class="d-inline">

                  		{{ csrf_field() }}
                      {{method_field('delete')}}
                  	
                  	<button class="btn btn-secondary"><i class="fa fa-trash">Delete</i></button>
                  	</form></a>
                  </td>
                </tr>
              
                @endforeach
              </tbody>
               
              </table>
          
    </div>

            </div>
            <!-- /.card-body -->
   

@endsection