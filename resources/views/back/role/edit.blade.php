  @extends('back.layout.master')
  @section('content')


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
       Update Role
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     <div class="row">
       <div class="col-md-10">
       
        <div class="box box-primary">
              <form method="POST" action="{{route('role.update',$data->id)}}">
             {{ csrf_field() }}
              {{ method_field('PUT')}}
               <div class="box-body">
                   <div class="form-group">
                    <label for="users">User</label>
                    <SELECT name="users" class="form-control" required="">
                     <option value="0">Select User</option>
                    @foreach($users as $value)
                    <option value="{{$value->id}}" @if ( $value->id==$data->users) selected @endif >{{$value->name}}</option>
                    @endforeach
                  </SELECT>
                  </div>

                    <div class="form-group">
                    <label for="usertype">UserType</label>
                    <SELECT name="usertype" class="form-control" required="">
                     <option value="0">Select UserType</option>
                    @foreach($usertype as $value)
                  <option value="{{$value->id}}" @if($value->id==$data->usertype) selected @endif> {{$value->name}}</option>
                    @endforeach
                  </SELECT>
                  </div>
        </div>
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
           </form>
              
          
       
            </div>
       
        </div>
      

    </section>
 
  </div>
           
               
             
              

              
             


 
  @endsection