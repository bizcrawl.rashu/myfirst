@extends('back.layout.master')
@section('content')




 <div class="content-wrapper">
      <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

  <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $i=1; @endphp
                @foreach($data as $key=>$d)
  		        <tr>
                  <td scope="row">{{$i}}}</td>
                  <td>{{$d->name}}</td>
                  <td>{{$d->status}}</td>
        
                  <td>
                  	<a href="{{route('usertype.edit',$d->id)}}" class="btn btn-secondary"><i class="fa fa-cogs"></i>Edit</a>
                  	  <a class="btn btn-secondary delete" style="border-color: none;"><form action="{{route('usertype.destroy',$d->id)}}" method="POST" class="d-inline">
                  		{{ csrf_field() }}
                  		{{method_field('delete')}}  
                <button class="btn btn-secondary" ><i class="fa fa-trash">Delete</i></button>
                  	</form>
                </a>
                  
                  </td>
                </tr>
                @php $i++; @endphp
                @endforeach
               </tbody>
              </table>
         </div>
       </div>

@endsection