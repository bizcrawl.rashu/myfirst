  @extends('back.layout.master')
  @section('content')


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Update UserType
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
       <div class="col-md-10">
       
        <div class="box box-primary">
              <form method="POST" action="{{route('usertype.update',$data->id)}}">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="box-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name='name' class="form-control" id="name" placeholder="Enter name here" value="{{$data->name}}">
                  </div>
                  <div class="form-group">
                    <label for="status">Status</label>
                      <div class="form-control">
                        <input type="radio" value="active" name="status"  id="status" @if($data->status=='active')checked @endif><span style="padding-left: 10px;">active</span>

                        <input type="radio" name="status" value="expired" style="margin-left: 50px;" id="status" @if($data->status=='expired')checked @endif ><span style="padding-left: 10px;">expired</span>

                         <input type="radio" name="status" value="disabled" style="margin-left: 50px;" id="status" @if($data->status=='disabled')checked @endif> <span style="padding-left: 10px;">disabled</span>
                      </div>
                  </div>
              </div>
            
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              </form>
            </div>
        </div>
      </div>
    </section>
  </div>
   @endsection
                 
                
               

               
            
              
              
            
              
            
        

      
  


 