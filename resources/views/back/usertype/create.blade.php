  @extends('back.layout.master')
  @section('content')


<div class="content-wrapper">
   <section class="content-header">
      <h1>
       Add usertype
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>
    
  <section class="content">
    <div class="row">
      <div class="col-md-10">
        <div class="box box-primary">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div> -->
            <form role="form" id="quickForm" method="post" action="{{route('usertype.store')}}">
              {{ csrf_field() }}
              <div class="box-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="heading" placeholder="Enter name here">
                  </div>
                  <div class="form-group">
                    <label for="status">Status</label>
                      <div class="form-control">
                        <input type="radio" value="active" name="status"  id="status"><span style="padding-left: 10px;">active</span>
                         <input type="radio" value="expired" name="status"  id="status"><span style="padding-left: 10px;">Expired</span>
                        <input type="radio" name="status" value="disable" style="margin-left: 50px;" id="is_active"> <span style="padding-left: 10px;">Disable</span>
                      </div>
                  </div>
              </div>
              
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>



            
                  
            
      
        
       
  @endsection