@extends('back.layout.master')
  @section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
      <section class="content-header">
      <h1>
       Update Photo
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-10">
          <div class="box box-primary">
           <form  method="post" action="{{route('photo.update',$photo->id)}}" enctype="multipart/form-data">
          
              {{ csrf_field() }}
                {{method_field('PUT')}}
              <div class="box-body">
             
                  <div class="form-group">
                    <label for="title">Photo Title</label>
                    <input type="text" name='title' class="form-control" id="title" value="{{$photo->title}}" placeholder="Enter the title of an photo here">
                  </div>
               

                  <div class="box box-primary">
                    <div class="box-header">
                    <label for="description" >Description</label>
                     
                    <textarea name="description" value="{{$photo->description}}" class="form-control ckeditor form-control-lg">{{$photo->description}}</textarea>
                  </div>
                </div>
                
                <div class="form-group">
                    <label for="photo" >Choose photo</label>
                    <input type="file" name='photo' value="{{$photo->photo}}" class="form-control" id="photo">{{$photo->photo}}
                  
                </div>
                     


             

                 
                </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
              </form>
        
            </div>
        
        </div>
     
      </div>
    </section>
   
  </div>

@endsection