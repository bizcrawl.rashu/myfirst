@extends('back.layout.master')
@section('content')

<div class="content-wrapper">
 <section class="content-header">
<div class="box-body">
	<table id="example2" class="table table-bordered table-hover">
		<a class="btn btn-primary" href="{{route('admin.album')}}" style="margin-bottom: 2%;">Go Back to Gallery</a>
		  <thead>
		  	  <tr>
               
                  <th>Image</th>
               
                  <th>Title</th>
                  <th>Action</th>
                 
                </tr>
                </thead>
                <tbody>
             
              
                   <tr>
            
                  <td><img class="thumbnail" src="{{asset('/storage/photos/($photo->id)/($photo->photo)')}}"alt="Click to make an update" ></td>
               	 <td>{{$photo->title}}</td>
                
                  <td>
                    
                     <a href="{{route('photo.edit',$photo->id)}}" class="btn btn-success"><i class="fa fa-cogs"></i>Edit</a>

                      <a class="btn btn-secondary" style="border-color: none;">
                      <form action="{{route('photo.destroy',$photo->id)}}" method="POST" class="d-inline">
                      {{ csrf_field() }}

                      <button class="btn btn-danger"><i class="fa fa-trash">Delete</i></button>
                  
                    </form>
                </a>
                  </td>

                </tr>
               
              </tbody>
            </table>
          </div>

	
	
		<!-- <div class="row">
			<div class="col-md-4">
				<img class="thumbnail" src="{{asset('/storage/photos/($photo->id)/($photo->photo)')}}" >
				<br>
				<h4 class="text-center">{{$photo->title}}</h4>
				 <a href="{{route('photo.edit',$photo->id)}}" class="btn btn-success"><i class="fa fa-cogs"></i>Edit</a>
                      <a class="btn btn-secondary" style="border-color: none;">
                    <form action="{{route('photo.destroy',$photo->id)}}" method="POST" class="d-inline">

                      {{ csrf_field() }}
                  
                    <button class="btn btn-danger"><i class="fa fa-trash">Delete</i></button>
                    </form>
                </a>
			</div>
		</div> -->
	</section>
</div>
				
			

@endsection 
	
				

				
				


		






