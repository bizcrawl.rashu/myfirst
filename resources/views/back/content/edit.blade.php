  @extends('back.layout.master')
  @section('content')


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
      <h1>
       Update Role
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
       <div class="col-md-10">
       
        <div class="box box-primary">
            <!-- jquery validation -->
           
            
              <form method="POST" action="{{route('content.update',$data->id)}}">
              {{ csrf_field() }}
              <!-- {{method_field('PUT')}} -->
               <div class="box-body">

                <div class="card-body">
                  <div class="form-group">
                    <label for="heading">Heading</label>
                    <input type="text" name='heading' class="form-control" id="heading" placeholder="Enter Heading here" value="{{$data->heading}}">
                  </div>
                 <!--  <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" name="slug" value="{{$data->slug}}" class="form-control" id="slug" placeholder="Enter slug here">
                  </div> -->
                <div class="box box-primary">
                  <div class="box-header">
                    <label for="description">Description</label>
                    <textarea name="description" class="form-control ckeditor form-control-lg" value="{{$data->description}}">{{$data->description}}</textarea>
                  </div>
                </div>

                   <div class="form-group">
                    <label for="created_by">Created_By</label>
                    <SELECT name="created_by" class="form-control" required="">
                     
                     @foreach($user as $value)
                     <option value="{{$value->id}}"> {{$value->name}} </option>
              
                    @endforeach
                  </SELECT>
                  </div>

                   <div class="form-group">
                    <label for="is_active">Is_Active</label>
                      <div class="form-control">
                        <input type="radio" value="active" name="is_active"  id="is_active" @if($data->is_active=='active') checked @endif><span style="padding-left: 10px;">active</span>

                        <input type="radio" name="is_active" value="inactive" style="margin-left: 50px;" id="is_active" @if($data->is_active=='inactive') checked @endif> <span style="padding-left: 10px;">inactive</span>
                      </div>
                  </div>

                 
                </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
              </form>
              
           
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


 
  @endsection