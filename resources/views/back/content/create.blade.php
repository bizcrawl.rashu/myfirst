  @extends('back.layout.master')
  @section('content')


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
      <section class="content-header">
      <h1>
       Add Role
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-10">
          <div class="box box-primary">
           <form role="form" id="quickForm" method="post" action="{{route('content.store')}}">
          
              {{ csrf_field() }}
              <div class="box-body">
             
                  <div class="form-group">
                    <label for="heading">Heading</label>
                    <input type="text" name='heading' class="form-control" id="heading" placeholder="Enter Heading here">
                  </div>
               

                  <div class="box box-primary">
                    <div class="box-header">
                    <label for="description" >Description</label>
                     
                    <textarea name="description" class="form-control ckeditor form-control-lg"></textarea>
                  </div>
                </div>


                   <div class="form-group">
                    <label for="created_by">Created_By</label>
                    <SELECT name="created_by" class="form-control" required="">
                 
                    @foreach($data as $value)
                
                  <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                  </SELECT>
                  </div>

                   <div class="form-group">
                    <label for="is_active">Is_Active</label>
                      <div class="form-control">
                        <input type="radio" value="active" name="is_active"  id="is_active"><span style="padding-left: 10px;">active</span>
                        <input type="radio" name="is_active" value="inactive" style="margin-left: 50px;" id="is_active"> <span style="padding-left: 10px;">inactive</span>
                      </div>
                  </div>

                 
                </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              </form>
           
        
            </div>
        
        </div>
     
      </div>
    </section>
   
  </div>


 
  @endsection