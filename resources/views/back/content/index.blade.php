@extends('back.layout.master')
@section('content')

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
               <tr>
                  <th>Id</th>
                  <th>Heading</th>
                  <!-- <th>Slug</th> -->
                  <th>Description</th>
                  <th>Created_by</th>
                  <th>Active</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               @php $i=1; @endphp
                @foreach($data as $key=>$d)
              <tr>
                  <td scope="row">{{$i}}}</td>
                  <td>{{$d->heading}}</td>
                  <!-- <td>{{$d->slug}}</td> -->
                  <td>{{$d->description}}</td>
                    <td>{{$d->User['name']}}</td>
                  <td>{{$d->is_active}}</td>
                  <td>
                    <a href="{{route('content.edit',$d->id)}}" class="btn btn-secondary"><i class="fa fa-cogs"></i>Edit</a>
                      <a class="btn btn-secondary" style="border-color: none;">
                    <form action="{{route('content.destroy',$d->id)}}" method="POST" class="d-inline">

                      {{ csrf_field() }}
                      <!-- {{method_field('DELETE')}} -->
                    <button class="btn"><i class="fa fa-trash">Delete</i></button>
                    </form>
                  </td>
                </tr>
                @php $i++; @endphp
                @endforeach
               
              </tbody>
            </table>
            </div>

      </div>


          

  

@endsection