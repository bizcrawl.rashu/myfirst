@extends('back.layout.master')
  @section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
      <section class="content-header">
      <h1>
       Add Role
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-10">
          <div class="box box-primary">
           <form role="form" id="quickForm" method="post" action="{{route('album.store')}}" enctype="multipart/form-data">
          
              {{ csrf_field() }}
              <div class="box-body">
             
                  <div class="form-group">
                    <label for="title">Album Name</label>
                    <input type="text" name='title' class="form-control" id="title" placeholder="Enter the name of an album here">
                  </div>
               

                  <div class="box box-primary">
                    <div class="box-header">
                    <label for="description" >Description</label>
                     
                    <textarea name="description" class="form-control ckeditor form-control-lg"></textarea>
                  </div>
                </div>

                <div class="form-group">
                    <label for="cover_image" >Choose photo</label>
                    <input type="file" name='cover_image' class="form-control" id="cover_image">
                  
                </div>
                 
                </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              </form>
           
        
            </div>
        
        </div>
     
      </div>
    </section>
   
  </div>

@endsection
                     

 

             
