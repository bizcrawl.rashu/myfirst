@extends('back.layout.master')
@section('content')


<div class="content-wrapper">
<h3>{{$album->name}}</h3>
<a class="btn btn-primary" href="{{route('admin.album')}}" style="margin-bottom: 2%;">Go Back</a>
<a class="btn btn-success"  href="{{route('admin.photo.gallery',$album->id)}}" style="margin-bottom: 2%;">Upload Photo to album</a>

@if(count($album->photos)>0)	
<?php
$colcount=count($album->photos);
$i=1;
?> 


<div id="photos">

	<div class="row text-center">
		@foreach($album->photos as $photo)
		@if($i == $colcount)
		
			<div class="col-md-3">
				
				 <a href="{{route('back.photos.index',$photo->id)}}"> 
					<img class="thumbnail" src="{{asset('/storage/photos/($photo->id)/($photo->photo)')}}" >
				</a>
				<br>
				<h4>{{$photo->title}}</h4>
				@else
				<div class="col-md-4">
		
				<a href="{{route('back.photos.index',$photo->id)}}"> 
					<img class="thumbnail" src="{{asset('/storage/photos/($photo->id)/($photo->photo)')}}" >
				</a>
				<br>
				<h4>{{$photo->title}}</h4>
				
				@endif

				@if($i%3==0)	

				</div>
			</div>
				<div class="row text-center">
					@else
				</div>
				@endif
				<?php $i++;?>
				@endforeach
	
		
	</div>
	
</div>
 
@else
<p> No Photos to display</p>
@endif



</div>
@endsection