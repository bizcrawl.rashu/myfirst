@extends('back.layout.master')
@section('content')

@if(count($albums)>0)	
<?php
$colcount=count($albums);
$i=1;
?>

<div class="content-wrapper">
<div id="albums">

	<div class="row text-center">
		@foreach($albums as $album)
		@if($i == $colcount)
	
			<div class="col-md-4">
				
				<a href="{{route('album.show',$album->id)}}">
					<img class="thumbnail" src="{{url('storage/album_covers/ . $album->cover_image')}}" alt="Click to add photos"/>
				</a>
				<br>
				<h4>{{$album->name}}</h4>

                    <a href="{{route('album.edit',$album->id)}}" class="btn btn-success"><i class="fa fa-cogs"></i>Edit</a>
                      <a class="btn btn-secondary" style="border-color: none;">
                    <form action="{{route('album.destroy',$album->id)}}" method="POST" class="d-inline">

                      {{ csrf_field() }}
                  
                    <button class="btn btn-danger"><i class="fa fa-trash">Delete</i></button>
                    </form>
                </a>
				@else

				
				<div class="col-md-4">
		
					<a href="{{route('album.show',$album->id)}}">
						<img class="thumbnail" src="{{asset('/storage/album_covers/ . $album->cover_image')}}" alt="Click to add photos">
					</a>
					</br>
						<h4>{{$album->name}}</h4>
						  <a href="{{route('album.edit',$album->id)}}" class="btn btn-success"><i class="fa fa-cogs"></i>Edit</a>
						   <a class="btn btn-secondary" style="border-color: none;">
                    <form action="{{route('album.destroy',$album->id)}}" method="POST" class="d-inline">

                      {{ csrf_field() }}
                      {{method_field('DELETE')}}

                    <button class="btn btn-danger"><i class="fa fa-trash">Delete</i></button>
                    </form>
               </a>

						@endif
						@if($i%3==0)	




						
				</div>

			</div>
				<div class="row text-center">
					@else
				</div>
				@endif
				<?php $i++;?>
				@endforeach
			</div>
		
	</div>
	
</div>

@else
<p> No albums to display</p>
@endif
</div>

@endsection