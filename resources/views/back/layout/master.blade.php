<!DOCTYPE html>
<html>
<head>
	@include('back.common.header')
	<title></title>
</head>


<body class="hold-transition skin-blue sidebar-mini">
	
		@include('back.common.navbar')
	
		<div class="col-md-12" style="margin-top: 15px; margin-left: 16%; margin-right: -15%; ">
      @if(session()->exists('message'))
      <div class="alert alert-success" role="alert">
        {{session('message')}}
        
      </div>
        @endif

</div>
		@include('back.common.sidebar')
	
		@yield('content')

		@include('back.common.footer')

	
</body>
</html>