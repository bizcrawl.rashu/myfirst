<!DOCTYPE html>
<html>
<head>
	@includes('back.common.header')
	<title></title>
</head>
<body>
	<div class="wrapper">
		@includes('back.common.navbar')
		@includes('back.common.sidebar')
		@yield('content')
		@includes('back.common.footer')
	</div>

</body>
</html>