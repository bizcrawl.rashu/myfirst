<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\str;
use App\User;


class Content extends Model
{

	public $timestamps=false;

	protected $table='content';
    protected $fillable=[
    	'heading', 'slug' , 'description' , 'created_by', 'is_active'

    ];


    public function User(){
    	return $this->hasOne('App\User','id', 'created_by');
    }
    public function setSlugAttribute($heading){
        $this->attributes['heading']=$heading;
    	$this->attributes['slug']=str::slug($heading);
    }
    
}
