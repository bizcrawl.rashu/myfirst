<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{

   
    public function create(){

    	return view ('Registration.create');
    }

    public function store(Request $request){
    	$this->validate(request(),[
    		'name' => 'required',
    		'email'=>  'required|email',
    		'password'=> 'required'
    	]);

    	$user=User::create([
    		'name'=>$request->name,
    		'email'=>$request->email,
    		'password'=>$request->password,
    		]);
    	// auth()->login($user);
    	return redirect()-> route('login');
    }
}
