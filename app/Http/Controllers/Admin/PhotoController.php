<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Photo;
use App\Album;



class PhotoController extends Controller
{
	public function index(){
   
    $photo=Photo::get();
    $album=Photo::with('Album')->get();


    return view('back.photos.index',compact('photo','album')); 
    // return view('back.photos.index',compact('photo','album'));   
    }

    public function gallery($id){

        $photo=Photo::with('Album')->get();    
        $album=Album::findorFail($id);
         // dd($album->id);
         return view('back.photos.create',compact('photo','album'));

        
     }
        
	
     
    public function store(Request $request,$id){
        // dd($request->all(),$id);
    	$this->validate($request,[
            'album_id'=>'required',
    		'title' =>'required',
    		'description'=>'required',
    		'photo' => 'image|max:1999',

    	]);

    	$filenameWithExt = $request->file('photo')->getClientOriginalName();

    	// get the filename only
    	$filename=pathinfo($filenameWithExt, PATHINFO_FILENAME);

    	 // get extension
    	$extension =$request->file('photo')->getClientOriginalExtension();


    	// create new file
    	$filenameToStore = $filename.'_'.time().'.'.$extension;

    	// upload image
    	$path= $request->file('photo')->storeAs('public/photos' .$request->album_id, $filenameToStore);

    // create Photo
    	 Photo::create([
    	 	'album_id'=>$request->album_id,
    	  	'title'=> $request->title,
    		'description'=>$request->description,
    		'size'=>$request->file('photo')->getClientSize(),
    		'photo'=>$filenameToStore
    		]);
         session()->flash('message',$request->title .' photo sucessfully saved....');
    	  // return redirect()->route('admin.album')->with('success','Album Created');
    		return redirect()->route('admin.album');

    } 
    public function show($id){
        $photo=Photo::findorFail($id);
        // $album=Album::findorFail($id);
        return view('back.photos.index',compact('photo'));
    }
 
      

    public function edit($id){
        $photo=Photo::find($id);
        $album=Album::get();
        return view('back.photos.edit',compact('photo','album'));

    }
    public function update(Request $request,$id){
      
        $message="Photo updated sucessfully";
         $this->validate($request,[
            'title' =>'required',
            'description'=>'required',
            'photo' => 'image|max:1999',

        
    ]);
        $filenameWithExt = $request->file('photo')->getClientOriginalName();

        // get the filename only
        $filename=pathinfo($filenameWithExt, PATHINFO_FILENAME);

         // get extension
        $extension =$request->file('photo')->getClientOriginalExtension();


        // create new file
        $filenameToStore = $filename.'_'.time().'.'.$extension;

        // upload image
        $path= $request->file('photo')->storeAs('public/photos' .$request->album_id, $filenameToStore);

        Photo::find($id)->update([
            'title'=> $request->title,
            'description'=>$request->description,
            'size'=>$request->file('photo')->getClientSize(),
            'photo'=>$filenameToStore
        

    ]);
        session()->flash('message',$request->title . ' photo sucessfully updated');
        return redirect()->route('admin.album');
}

       public function destroy(Request $request,$id){
        $message="Photo deleted sucessfully";
            $photo=Photo::findorFail($id);
            $photo->delete();
             session()->flash('message',' Data sucessfully deleted');
            return redirect()->route('admin.album');
        
}

    
 }
