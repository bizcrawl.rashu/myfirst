<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Album;


class AlbumController extends Controller
{


	public function index(){
		$albums=Album::with('Photos')->get();
		return view('back.album.index')->with('albums',$albums);	
	}
    public function create(){
    	$data=Album::get();
    	return view('back.album.create',compact('data'));
    }

    public function store(Request $request){
    	$this->validate($request,[
    		'title' =>'required',
    		'description'=>'required',
    		'cover_image' => 'image|max:1999',
    	]);


    	$filenameWithExt = $request->file('cover_image')->getClientOriginalName();

    	// get the filename only
    	$filename=pathinfo($filenameWithExt, PATHINFO_FILENAME);

    	 // get extension
    	$extension =$request->file('cover_image')->getClientOriginalExtension();


    	// create new file
    	$filenameToStore = $filename.'_'.time().'.'.$extension;

    	// upload image
    	$path= $request->file('cover_image')->storeAs('public/album_covers',$filenameToStore);

    // create album_cover
    	 Album::create([
    	  	'name'=> $request->title,
    		'description'=>$request->description,
    		'cover_image'=>$filenameToStore
    		]);
    	session()->flash('message',$request->title  .' album sucessfully saved....');

    	return redirect()->route('admin.album');

    }
    public function show(Request $request,$id){
    	$album=Album::with('Photos')->find($id);
    	return view('back.album.show')->with('album',$album);

    	// return redirect()->route('back.album.show')->with('album',$album);
}

   public function edit($id){
   	$album=Album::findorFail($id);

   	return view('back.album.edit',compact('album'));
   }

   public function update(Request $request,$id){
   	$message="Album updated sucessfully";
   		$this->validate($request,[
    		'title' =>'required',
    		'description'=>'required',
    		'cover_image' => 'image|max:1999',
    	
   	]);
   		$filenameWithExt = $request->file('cover_image')->getClientOriginalName();

    	// get the filename only
    	$filename=pathinfo($filenameWithExt, PATHINFO_FILENAME);

    	 // get extension
    	$extension =$request->file('cover_image')->getClientOriginalExtension();


    	// create new file
    	$filenameToStore = $filename.'_'.time().'.'.$extension;

    	// upload image
    	$path= $request->file('cover_image')->storeAs('public/album_covers',$filenameToStore);

   		Album::find($id)->update([
		'name'=>$request->title,
		'description'=>$request->description,
		'cover_image'=>$filenameToStore
   		

   	]);
      session()->flash('message',$request->title  .' album sucessfully updated....');
   		return redirect()->route('admin.album')->with('status',$message);
}

	public function destroy(Request $request,$id){
		// $message="Album deleted sucessfully";
   			$album=Album::findorFail($id);
   			$album->delete();
        session()->flash('message',' Album sucessfully deleted');
   			return redirect()->route('admin.album');
   		
}
}
