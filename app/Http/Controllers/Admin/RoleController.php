<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\UserType;

class RoleController extends Controller
{
   public function index(){
   	$data=Role::paginate(6);
   	return view('back.role.index',compact('data'));
   }

   public function create(){
   	$data=Role::get();
      $users=User::get();
      $usertype=UserType::get();
   	//dd($data);
   	return view('back.role.create',compact('data','users','usertype'));
   }

   public function store(Request $request){

   	$message="Role create sucessfully";
   	$this->validate($request,[
   		'users'=>'required',
   		'usertype'=>'required'

   	]);
  	// dd($request->all(),$id);
   	Role::create([
   		'users'=>$request->users,
   		'usertype'=>$request->usertype

   	]);
   	return redirect()->route('admin.role')->with('status',$message);

   }
   public function edit($id){
   	$data=Role::findorFail($id);
   	$users=User::get();
   	$usertype=UserType::get();

   	return view('back.role.edit',compact('data','users','usertype'));
   }
   public function update(Request $request,$id){
   	$message="Role updated sucessfully";
   		$this->validate($request,[
   		'users'=>'required',
   		'usertype'=>'required'

   	]);

   		Role::find($id)->update([
		'users'=>$request->users,
   		'usertype'=>$request->usertype


   	]);
   		return redirect()->route('admin.role')->with('status',$message);
}

   		public function destroy(Request $request,$id){
   		$message="Role deleted sucessfully";
   			$data=Role::findorFail($id);
   			$data->delete();

   			return redirect()->route('admin.role')->with('status',$message);
   		}

   }










