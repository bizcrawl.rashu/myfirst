<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\UserType;

class UserTypeController extends Controller
{

    public function index(){
    	$data=UserType::paginate(6);
    	return view('back.usertype.index',compact('data'));
           	
    }

    public function create(){
    	$data=UserType::get();
    	return view('back.usertype.create',compact('data'));

    }
    public function store(Request $request){
    	$message="User detail Created";
        
    	$this->validate($request,[
    		'name'=>'required',
    		'status'=>'required',
    	
    	]);
    	
    	UserType::create([
    		'name'=>$request->name,
    		'status'=>$request->status
    	
    	]);
    	return redirect()->route('admin.usertype')->with('status',$message);

    }
    public function edit($id){
    	$data=UserType::findorFail($id);
        
    	return view('back.usertype.edit',compact('data'));
    }

    public function update(Request $request,$id){
    	$message="UserType updated successfully";

    	$this->validate($request,[
    		'name'=>'required',
    		'status'=>'required',
    		
    	]);
    	UserType::find($id)->update([
    		'name'=>$request->name,
    		'status'=>$request->status,
    		   
    	]);
    	return redirect()->route('admin.usertype')->with('status',$message);
    }

    public function destroy($id){

        
        $message="UserType deleted successfully";

        // $data=UserType::findorFail($id);

        // $data=delete();
        
        DB::table('usertype')->where('id', $id)->delete();
        return redirect()->route('admin.usertype')->with('status',$message);
    }
}
