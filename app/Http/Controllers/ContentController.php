<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Content;
use App\User;
use Illuminate\Support\str;

class ContentController extends Controller
{


    public function create(){
    	$data=User::get();
    	return view('back.content.create',compact('data'));
    }

    
   
    public function store(Request $request){

    	$message="Content Created sucessfully";
    	Content::create([
    		'heading'=>$request->heading,
             'slug' => $request->heading,
    		'description'=>$request->description,
    		'created_by'=>$request->created_by,
    		'is_active'=>$request->is_active
    
    	]);

    	$this->validate($request,[
    		'heading'=>'required',
    		// 'slug'=>'required',
    		'description'=>'required',
    		'created_by'=>'required',
    		'is_active'=>'required'

    	]);
    	 return redirect()->route('content.index')->with('status',$message);
    
}
  

    public function index(){
	$data=Content::get();
	return view('back.content.index',compact('data'));
    }
	
	public function edit($id)
	{
		$data=Content::findorFail($id);
		$user=User::get();
		 // return view('back.content.edit')->withContent('data')    
                                        // ->withUser('user');

     return view('back.content.edit',compact('data','user'));
                                     
	}

	public function update(Request $request,$id){
     
        //dd($request->all(),$id);
		$message="Content Updated Successfully";

        $this->validate($request,[
            'heading'=>'required',
        // 'slug'=>'required',  
            'description'=>'required',
            'created_by'=>'required',
            'is_active'=>'required'

        ]);
        Content::findorFail($id)->update([

            'heading'=>$request->heading,
            'slug' => $request->heading,
            'description'=>$request->description,
            'created_by'=>$request->created_by,
            'is_active'=>$request->is_active

        ]); 

		
	return redirect()->route('content.index')->with('status',$message);
		
	}
	public function destroy(Request $request,$id){
        //dd($request->all(),$id);

		$message="Content deleted Successfully";
		$data=Content::findorFail($id);
		$data->delete();

		return redirect()->route('content.index')->with('status',$message);
	}

}



