<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	public $timestamps=false;
    protected $table='role';
    protected $fillable=['users','usertype'];

public function User(){
	return $this->hasOne('App\User','id','users');
}
public function UserType(){
	return $this->hasOne('App\UserType','id','usertype');
}
}

