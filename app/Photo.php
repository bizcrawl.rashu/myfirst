<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Album;

class Photo extends Model
{
	protected $table='photos';
    protected $fillable=[
    	'album_id','photo','title','size','description'
    ];
    public function Album(){
    	// return $this->belongsTo('App\Album','id');
    	return $this->belongsTo('App\Album','id', 'album_id');
    }
  

}
