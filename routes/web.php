<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

// Route::get('/index','HomeController@index')->name('index');
// Route::get('/home', 'HomeController@home')->name('home');
Route::get('/dash','HomeController@dash')->name('dash');
Route::get('/logout','HomeController@logout')->name('logout');


Route::get('/register', function(){
	return redirect('/home');
});

Route::get('/register','RegisterController@create')->name('register');
Route::post('/register','RegisterController@store');

Route::get('/content','ContentController@create')->name('content.create');
Route::post('/content','ContentController@store')->name('content.store');

Route::get('/index','ContentController@index')->name('content.index');
// Route::post('/update/{id}',['uses'=>'ContentController@update','as'=>'content.update']);
Route::post('update/{id}',['as'=> 'content.update','uses'=>'ContentController@update']);


Route::post('/delete/{id}',['uses'=>'ContentController@destroy','as'=>'content.destroy']);

Route::get('edit/{id}',['as'=> 'content.edit','uses'=>'ContentController@edit']);
 


Route::group(['namespace'=>'Admin','prefix'=>'admin','middleware'=>['auth:web']],
function()
{
Route::get('/','UserTypeController@index')->name('index');

Route::resource('usertype','UserTypeController');

Route::get('/usertype','UserTypeController@index')->name('admin.usertype');
Route::post('/usertype','UserTypeController@store')->name('usertype.store');


Route::get('/','RoleController@index')->name('index');

Route::resource('role','RoleController');
Route::get('/role','RoleController@index')->name('admin.role');


Route::get('/','AlbumController@index')->name('index');
Route::resource('album','AlbumController');
Route::get('/album','AlbumController@index')->name('admin.album');
Route::get('/album/{id}',['as'=> 'album.show','uses'=>'AlbumController@show']);
Route::get('edit/{id}',['as'=> 'album.edit','uses'=>'AlbumController@edit']);

// Route::post('update/{id}',['as'=> 'album.update','uses'=>'AlbumController@update']);
Route::post('album/{id}',['as'=> 'album.destroy','uses'=>'AlbumController@destroy']);



Route::get('/','PhotoController@index')->name('index');
Route::resource('/photo','PhotoController');
Route::get('/photo/{id}','PhotoController@index')->name('admin.photo');

Route::post('/photo-store/{id}',['as'=>'photo.store','uses'=>'PhotoController@store']);

Route::get('/photo-gallery/{id}','PhotoController@gallery')->name('admin.photo.gallery');
Route::get('/photo/{id}','PhotoController@show')->name('back.photos.index');
Route::get('edit/{id}',['as'=> 'photo.edit','uses'=>'PhotoController@edit']);

// Route::post('update/{id}',['as'=> 'photo.update','uses'=>'PhotoController@update']);

Route::post('photo/{id}',['as'=> 'photo.destroy()','uses'=>'PhotoController@destroy']);

});





