<?php

use Illuminate\Database\Seeder;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \Illuminate\Support\Facades\DB::table('usertype')->insert([
            'id'=>'2',
            'name' => 'admin',
            'status' => 'active',
            
        ]);
    }
}
