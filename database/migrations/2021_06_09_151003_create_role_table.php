<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('role', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('users');
            $table->unsignedBigInteger('usertype');
            $table->foreign('users')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('usertype')->references('id')->on('usertype')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('role');
    }
}
